$(document).ready(function(){

    $( window ).load(function() {
        if (window.location.hash && window.location.hash == '#sign' || window.location.hash == '#idea') {
            setTimeout(showSignup, 500);
        }
    });

    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
            || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length && !target.hasClass('modal')) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('#signupModal').on('show.bs.modal', function (e) {
        if (location.protocol != 'https:') {
            e.preventDefault();

            $('#redirectModal').modal('show');
            return false;
        }

        initSignup();
    });

    $('#redirectModal').on('shown.bs.modal', function (e) {
        setTimeout(function(){
            var url = 'https://www-refreshav.rhcloud.com/';

            if (window.location.hostname == 'rav.org') {
                var url = 'https://rav.org/';
            }

            if (window.location.hash) {
                url += window.location.hash;
            } else {
                url += '#sign';
            }

            window.location.replace(
                url
            );
        }, 1500);
    });

    $.getScript(
        '/tumblr',
        function() {
            if (!tumblr_api_read || !tumblr_api_read.posts) {
                return;
            }

            var blogContainer = $('.blog-items'),
                items = '';

            blogContainer.empty();

            for (var c = 0, i = 0, l = tumblr_api_read.posts.length; i < l; ++i) {
                if (!tumblr_api_read.posts[i]['regular-title']) {
                    continue;
                }

                items += '<p class="lead"><i class="fa fa-bolt"></i> <a href="' + tumblr_api_read.posts[i]['url-with-slug'] + '">' + tumblr_api_read.posts[i]['regular-title'] + '</a></p>';

                if (++c == 10) {
                    items += '<p class="lead"><i class="fa fa-bolt"></i> <a href="http://f5food.tumblr.com/">More ...</a></p>';
                    break;
                }
            }

            if (items.length == 0) {
                blogContainer.append('<p class="lead">Nothin Happening</p>');
            } else {
                blogContainer.append(items);
            }
        }
    );

    $('body').tooltip({
        selector: "[data-toggle=tooltip]"
    });

    setInterval(rotateIdeas, 2000);
});

lastIdeaNo = 0;

function rotateIdeas() {
    var $pitchIdeas = $('#pitch-ideas li'),
        ideaNo = getRandomInt(0, $pitchIdeas.length - 1);

    if (lastIdeaNo == ideaNo) {
        ideaNo = getRandomInt(0, $pitchIdeas.length - 1);
    }

    $('#pitch-idea').text($pitchIdeas.eq(ideaNo).text());

    lastIdeaNo = ideaNo;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function showSignup() {
    initSignup();
    $('#signupModal').modal('show');
}

function initSignup() {
    var $signupModal = $('#signupModal');

    if ($signupModal.data('inited') == 1) {
        return;
    }

    $signupModal.find('.alert').hide();

    var $haveIdea = $('#have-idea');

    if (window.location.hash && window.location.hash == '#idea') {
        $haveIdea.attr('checked', 'checked');
    }

    if ($haveIdea.is(':checked')) {
        $('#idea-group').show();
    } else {
        $('#idea-group').hide();
    }

    $signupModal.find('select').select2();

    $haveIdea.change(function(){
        if($(this).is(':checked')) {
            $('#idea-group').show('fast');
        } else {
            $('#idea-group').hide('slow');
        }
    });

    $('input[name=rate]').change(function(){
        if ($('input[name=rate]:checked').val() == 'onlooker') {
            $('#idea-group, #have-idea-group').hide('slow');
            var $type = $('#type'),
                types = $type.select2('val');

            if (types) {
                types.push('onlooker');
            } else {
                types = 'onlooker';
            }

            $type.select2('val', types);
        } else {
            $('#have-idea-group').show('fast');

            if ($('#have-idea').is(':checked')) {
                $('#idea-group').show('fast');
            }
        }
    });

    $signupModal.find('form').validate(
        {
            highlight: function(element) {
                $(element)
                    .closest('.form-group')
                    .removeClass('has-success')
                    .addClass('has-error')
                    .find('.help-block')
                    .removeClass('hidden');
            },
            success: function(element) {
                element
                    .text('')
                    .addClass('hidden')
                    .closest('.form-group')
                    .removeClass('has-error')
                    .addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            submitHandler: function(form, e) {
                var $form = $(form),
                    $button = $form.find('button');

                $signupModal.find('.alert').hide();

                $button.attr('disabled', 'disabled').children('i').addClass('fa-spin');

                e.preventDefault();

                $('#stripeToken').remove();

                try {
                    Stripe.card.createToken({
                        number: $('#number').val(),
                        cvc: $('#cvc').val(),
                        exp_month: $('#exp-month').val(),
                        exp_year: $('#exp-year').val(),
                        name: $('#name').val()
                    }, function(status, response){
                        if (response.error) {
                            $signupModal.find('.alert').show().text(response.error.message);
                            $button.attr('disabled', '').children('i').removeClass('fa-spin');
                        } else {

                            $form.append(
                                '<input type="hidden" id="stripeToken" name="stripeToken" value="' + response.id + '"/>'
                            );

                            $.post(
                                    '/',
                                    {
                                        name: $('#name').val(),
                                        email: $('#email').val(),
                                        type: $('#type').val(),
                                        'have-idea': $('#have-idea').val(),
                                        idea: $('#idea').val(),
                                        rate: $('input[name=rate]:checked').val(),
                                        stripeToken: $('#stripeToken').val()
                                    }
                                )
                                .done(
                                function(result) {
                                    if (result && result.result) {
                                        $('#number').val('');
                                        $('#cvc').val('');
                                        $('#exp-month').val('');
                                        $('#exp-year').val('');
                                        $('#stripeToken').remove();

                                        $('#signupModal').modal('hide');
                                        $('#signupSuccessModal').modal('show');
                                        $button
                                            .attr('disabled', null)
                                            .children('i')
                                            .removeClass('fa-spin');
                                    } else {
                                        $button
                                            .attr('disabled', null)
                                            .children('i')
                                            .removeClass('fa-spin');

                                        var message = false

                                        if (result.message) {
                                            message = result.message;
                                        }

                                        var error = '<div class="alert alert-danger alert-block">' +
                                            '<button type="button" class="close" data-dismiss="alert"><strong>&times;</strong></button>'+
                                            '<h4>Yikes!</h4>';

                                        if (message) {
                                            error += '<p>Something seems to be misbehaving.  This is what we know so far:</p>' +
                                                '<p><strong>' + message + '</strong></p>' +
                                                '<p>Can you drop us a good old fashioned <a href="mailto:food@f5av.org">email</a> if you need a hand?</p>';
                                        } else {
                                            error += '<p>Something seems to be misbehaving.  Soo sorry.  Can you drop us a good old fashioned <a href="mailto:food@f5av.org">email</a> to tell us how you blew up our website?</p>';
                                        }

                                        error += '</div>';

                                        $(form).parent().prepend(error);
                                    }
                                }
                            )
                                .fail(
                                function() {

                                    $button
                                        .attr('disabled', null)
                                        .children('i')
                                        .removeClass('fa-spin');

                                    var error = '<div class="alert alert-danger alert-block">' +
                                        '<button type="button" class="close" data-dismiss="alert"><strong>&times;</strong></button>'+
                                        '<h4>Yikes!</h4>' +
                                        '<p>Something seems to be misbehaving.  Soo sorry.  Can you drop us a good old fashioned <a href="mailto:food@f5av.org">email</a> to tell us how you blew up our website?</p>' +
                                        '</div>';

                                    $(form).parent().prepend(error);
                                }
                            );
                        }
                    });
                } catch (exception) {
                    $signupModal.find('.alert').show().text(exception.message);
                    $button.attr('disabled', '').children('i').removeClass('fa-spin');
                }

                return false;
            }
        }
    );

    $signupModal.data('inited', 1);
};

var _gaq=[['_setAccount','UA-37580538-1'],['_trackPageview']];
(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src='//www.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
