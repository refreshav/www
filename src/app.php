<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require 'bootstrap.php';

$app->get(
    '/food',
    function(Silex\Application $app){
        return $app->redirect('http://food.refreshannapolisvalley.org/');
    }
);

$app->get(
    '/',
    function(Silex\Application $app) {
        return $app['twig']->render(
            'food.twig',
            array(
                'STRIPE_PUBLIC_KEY' => getenv('STRIPE_PUBLIC_KEY')
            )
        );
    }
);

$app->post(
    '/',
    function(Silex\Application $app) {
        $request = $app['request'];

        switch ($_POST['rate']) {
            case 'regular' :
                $amount = 40.00;
                $description = 'Regular registration for F5://Food';
                $template = 'registered.twig';
                break;
            case 'student' :
                $amount = 20.00;
                $description = 'Student registration for F5://Food';
                $template = 'registered.twig';
                break;
            case 'onlooker' :
                $amount = 10.00;
                $description = 'Onlooker registration for F5://Food';
                $template = 'registered-onlooker.twig';
                break;
            default :
                $amount = 40.00;
                $description = 'Regular registration for F5://Food';
                $template = 'registered.twig';
        }

        try {
            Stripe::setApiKey(getenv('STRIPE_PRIVATE_KEY'));

            $charge = Stripe_Charge::create(
                array(
                    'amount' => $amount * 100,
                    'currency' => 'cad',
                    'card' => $_POST['stripeToken'],
                    'description' => $description,
                    'metadata' => array(
                        'name' => $_POST['name'],
                        'email' => $_POST['email'],
                        'type' => implode(', ', $_POST['type']),
                    )
                )
            );

            unset($_POST['stripeToken']);
            $_POST['charge-id'] = $charge->id;

        } catch (Exception $exception) {
            return new Response(
                json_encode(
                    array(
                        'result' => false,
                        'message' => $exception->getMessage()
                    )
                ),
                200,
                array('Content-Type' => 'application/json')
            );
        }

        try {

            $mandrill = new Mandrill(getenv('MANDRILL_KEY'));

            $mandrill->messages->send(
                array(
                    'html' => '<pre>'. print_r($_POST, 1) . '</pre>',
                    'subject' => 'F5://Food',
                    'from_email' => $request->get('email'),
                    'from_name' => $request->get('name'),
                    'to' => array(
                        array(
                            'email' => 'refreshannapolisvalley@gmail.com',
                            'name' => 'F5://AV'
                        )
                    ),
                    'important' => true,
                    'tags' => array('F5AV')
                )
            );

            $mandrill = new Mandrill(getenv('MANDRILL_KEY'));

            $qr = 'Welcome ' . $_POST['name'] . "!\r\n\r\n" . implode(', ', $_POST['type']) . ', ' . $_POST['rate'];

            if (!empty($_POST['have-idea'])) {
                $qr .= ', and will pitch!';
            }

            $mandrill->messages->send(
                array(
                    'html' => $app['twig']->render(
                        $template,
                        array(
                            'name' => $_POST['name'],
                            'email' => $_POST['email'],
                            'type' => implode(', ', $_POST['type']),
                            'qr' => $qr
                        )
                    ),
                    'subject' => 'See You at F5://Food',
                    'from_email' => 'food@f5av.org',
                    'from_name' => 'F5://AV',
                    'to' => array(
                        array(
                            'email' => $request->get('email'),
                            'name' => $request->get('name')
                        )
                    ),
                    'important' => true,
                    'tags' => array('F5AV-Registered')
                )
            );

            return new Response(
                json_encode(
                    array(
                        'result' => true,
                        'message' => 'Done'
                    )
                ),
                200,
                array('Content-Type' => 'application/json')
            );
        } catch (Exception $exception) {
            return new Response(
                json_encode(
                    array(
                        'result' => false,
                        'message' => $exception->getMessage()
                    )
                ),
                200,
                array('Content-Type' => 'application/json')
            );
        }
    }
);

$app->get('/tumblr', function(Silex\Application $app){
    return new Response(
        file_get_contents('http://f5food.tumblr.com/api/read/json'),
        200,
        array(
            'Content-Type' => 'text/javascript'
        )
    );
});
